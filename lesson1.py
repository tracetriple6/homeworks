import functools
from collections import OrderedDict


def cache(maxsize=0):
    if isinstance(maxsize, int):
        if maxsize <= 0:
            maxsize = 1
        elif maxsize >= 1:
            maxsize += 1
    else:
        raise Exception
    def _cache(f):
        @functools.wraps(f)
        def deco(*args):
            if args in deco._cache:
                return deco._cache[args]

            result = f(*args)
            deco._cache[args] = result

            if len(deco._cache) >= maxsize > 1:
                deco._cache.popitem(last=False)
            return result

        deco._cache = OrderedDict()
        return deco

    return _cache


@cache(maxsize=0)
def fibo(n):
    """Super inefficient fibo function"""
    if n < 2:
        return n
    else:
        return fibo(n-1) + fibo(n-2)


print(fibo(120))